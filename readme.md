```
USAGE
       passwordIsValid [user] [password]

NOTES
       • If 'user' is not entered the current user is assumed.

       •  If no 'user' and 'password' is entered they are asked interactively,
       and the result is printed as 'valid' or 'invalid'.

       • If you can see the password while typing it, it  means  that  way  of
       passing  it is insecure. That it will be recored into the terminal his‐
       tory, and anyone reading that history will be able to steal it.

       • On your terminal programs you can securely ask for a  password  with:
       echo 'Type the password and press enter'; read -rs password.

       •  This  program requires root priviledges to work. You can get them by
       typing 'sudo -v' or 'su' before entering this  command.  You  can  keep
       'sudo'  priviledges  active by running 'sudo -v' periodically on a dif‐
       ferent process.
